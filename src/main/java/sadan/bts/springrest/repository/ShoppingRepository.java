package sadan.bts.springrest.repository;

/**
 * Created by Sadan Fitroni on 15/10/2020.
 */

import org.springframework.data.jpa.repository.JpaRepository;
import sadan.bts.springrest.model.Shopping;
public interface ShoppingRepository extends JpaRepository<Shopping, Integer> {

}
