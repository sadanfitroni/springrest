package sadan.bts.springrest.service;
/**
 * Created by Sadan Fitroni on 15/10/2020.
 * */

 import sadan.bts.springrest.model.Shopping;
import sadan.bts.springrest.repository.ShoppingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ShoppingService {
    @Autowired
    private ShoppingRepository shoppingRepository;
    public List<Shopping> getAllShopping() {
        return shoppingRepository.findAll();
    }

    public void createNewShopping(Shopping shopping) {
        shoppingRepository.save(shopping);
    }

    public Shopping getShoppingById(Integer id) {
        return shoppingRepository.findById(id).get();
    }

    public void deleteShopping(Integer id) {
        shoppingRepository.deleteById(id);
    }
}
