package sadan.bts.springrest.controller;

/**
 * Created by Sadan Fitroni on 15/10/2020.
 */
import sadan.bts.springrest.model.User;
import sadan.bts.springrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserService userService;

    // signup users
    @PostMapping("/signup")
    public void add(@RequestBody User user) {
        userService.signUp(user);
    }
    // get all users
    @GetMapping("")
    public List<User> list() {
        return userService.getAllUsers();
    }
}
