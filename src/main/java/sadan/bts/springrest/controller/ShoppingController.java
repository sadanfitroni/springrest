package sadan.bts.springrest.controller;

/**
 * Created by Sadan Fitroni on 15/10/2020.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sadan.bts.springrest.model.Shopping;
import sadan.bts.springrest.service.ShoppingService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/shopping")
public class ShoppingController {
    @Autowired
    ShoppingService shoppingService;

    // create new shopping
    @PostMapping("")
    public void add(@RequestBody Shopping shopping) {
        shoppingService.createNewShopping(shopping);
    }
    // get all shopping
    @GetMapping("")
    public List<Shopping> list() {
        return shoppingService.getAllShopping();
    }

    // get shopping byId
    @GetMapping("/{id}")
    public ResponseEntity<Shopping> get(@PathVariable Integer id) {
        try {
            Shopping shopping = shoppingService.getShoppingById(id);
            return new ResponseEntity<Shopping>(shopping, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Shopping>(HttpStatus.NOT_FOUND);
        }
    }

    // delete shopping byId
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        shoppingService.deleteShopping(id);
    }
}
