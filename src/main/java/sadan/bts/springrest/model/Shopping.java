package sadan.bts.springrest.model;
/**
 * Created by Sadan Fitroni on 15/10/2020.
 */

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "shopping")
public class Shopping {
    private int id;
    private Date createdDate;
    private String name;
    public Shopping() {
    }

    public Shopping(int id, Date CreatedDate, String name) {
        this.id = id;
        this.createdDate = CreatedDate;
        this.name = name;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

